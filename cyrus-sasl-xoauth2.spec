#
# spec file for package cyrus-sasl
#
# Copyright (c) 2021 SUSE LLC
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.


Name:           cyrus-sasl-xoauth2
Version:        0.2
Release:        0
URL:            https://github.com/moriyoshi/cyrus-sasl-xoauth2
Summary:        XOAUTH2 mechanism plugin for cyrus-sasl
License:        MIT
Source:         https://github.com/moriyoshi/cyrus-sasl-xoauth2/archive/refs/tags/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
Patch1:         wreturntype-error.patch
Patch2:         use-libdir.patch
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  cyrus-sasl-devel
BuildRequires:  cyrus-sasl

%description
This is a plugin implementation of XOAUTH2.

FYI: if you are forced to use XOAUTH2-enabled SMTP / IMAP servers by your
employer and want to keep using your favorite *nix MUA locally, the following
detailed document should help a lot:
http://mmogilvi.users.sourceforge.net/software/oauthbearer.html

%prep
%setup -q
%autopatch -p1

%build
bash autogen.sh
export CFLAGS="%optflags -fno-strict-aliasing"
%configure --with-pic \
           --with-plugindir=%{_libdir}/sasl2 \
           --with-configdir=/etc/sasl2/:%{_libdir}/sasl2 \
           --with-saslauthd=/run/sasl2/ \
           --enable-pam \
           --disable-static \
           --with-devrandom=/dev/urandom
%{__make} %{?_smp_mflags} sasldir=%{_libdir}/sasl2

%install
make DESTDIR=$RPM_BUILD_ROOT sasldir=%{_libdir}/sasl2 install

%files
%doc README.md
%license COPYING
%_libdir/sasl2/libxoauth2.so
%_libdir/sasl2/libxoauth2.so.0
%_libdir/sasl2/libxoauth2.so.0.0.0

%changelog
